#include "pch.h"
#include "SimulatedAnnealing.h"




SimulatedAnnealing::SimulatedAnnealing(int ** matrix, int sizeOfMatrix)
{
	timer = new Timer();
	this->matrix = matrix;
	this->sizeOfMatrix = sizeOfMatrix;
	temperature = sizeOfMatrix;
	decreaser = 0.95;
}

SimulatedAnnealing::~SimulatedAnnealing()
{
	if (timer != NULL)
		delete timer;
}

TSPSolution * SimulatedAnnealing::solveTSP(double timeToStop)
{
	TSPSolution* x = NULL;
	TSPSolution* y = NULL;
	TSPSolution* optimum = NULL;
	int optimumSolution;
	int counterForDiversification = 0;

	x = new TSPSolution(sizeOfMatrix);
	x->generateRandomSoltion();
	optimum = x;
	optimumSolution = optimum->calculateSolution(matrix);
	timer->start();


	do {

		y = randMove(x);

		if (randProbality() < propability(x, y)) {
			if (x != optimum)
				delete x;

			x = y;

			if (optimumSolution > x->calculateSolution(matrix)) {
				delete optimum;
				optimum = x;
				optimumSolution = optimum->calculateSolution(matrix);
				counterForDiversification = 0;
			}
		}
		else
			delete y;

	} while (!timer->stop(timeToStop));

	return optimum;
}


void SimulatedAnnealing::decreaseTempertaure()
{
	temperature = temperature * decreaser;
}

TSPSolution * SimulatedAnnealing::randMove(TSPSolution * x)
{
	TSPSolution* y = new TSPSolution(x->getTab(), sizeOfMatrix);

	int i;
	int j;

	do {
		i = random(1, sizeOfMatrix - 1);
		j = random(1, sizeOfMatrix - 1);
	} while (i == j);

	y->swap(i, j);

	return y;
}


float SimulatedAnnealing::randProbality()
{
	int var1 = random(0, 100);
	int var2 = random(0, 100);
	float prob;

	if (var1 > var2)
		prob = (float)var2 / (float)var1;
	else
		prob = (float)var1 / (float)var2;

	return prob;
}

float SimulatedAnnealing::propability(TSPSolution * x, TSPSolution * y)
{
	float wykladnik = -((float)(y->calculateSolution(matrix) - x->calculateSolution(matrix)) / temperature);
	float prob = exp(wykladnik);

	if (prob < 1)
		return prob;
	else
		return 1;
}

int SimulatedAnnealing::random(int from, int to)
{
	random_device rd; // non-deterministic generator
	mt19937 gen(rd()); // random engine seeded with rd()
	uniform_int_distribution<> dist(from, to); // distribute results between
	 // 1 and 1000000 inclusive

	return dist(gen);
}
