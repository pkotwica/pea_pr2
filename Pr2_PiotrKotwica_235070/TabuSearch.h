#pragma once
#include <iostream>
#include <random>
#include "TSPSolution.h"
#include "Timer.h"
#include "Tabu.h"

using namespace std;

class TabuSearch
{
	Timer* timer;
	int** matrix;
	int sizeOfMatrix;
	int limitForDiversification;


public:
	TabuSearch(int** matrix, int sizeOfMatrix);
	~TabuSearch();

	TSPSolution* solveTSP(double timeToStop, bool dywersyfikacja);
	TSPSolution* findNeightbour(Tabu* tabuList, TSPSolution* x, int optimumSolution);
	

};

