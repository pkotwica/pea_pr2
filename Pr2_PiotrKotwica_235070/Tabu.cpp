#include "pch.h"
#include "Tabu.h"


Tabu::Tabu(int sizeOfProblem)
{
	int head = NULL;
	int tail = NULL;
	int cadency = (int)floor(0.3*(double)sizeOfProblem);
}


Tabu::~Tabu()
{
	Element *nxt; //zapamietanie nastepnej struktury

	while (head != NULL)	//usuwanie od poczatku struktur
	{
		nxt = head->next;
		delete head;
		head = nxt;
	}
	head = NULL;
	tail = NULL;
}

bool Tabu::contains(int x, int y)
{
	Element* ptr = head;

	while (ptr != NULL) {
		if ((ptr->x == x && ptr->y == y) || (ptr->x == x && ptr->y == y))
			return true;

		ptr = ptr->next;
	}

	return false;
}

void Tabu::add(int x, int y)
{
	if (head == NULL)
	{
		head = new Element;
		tail = head;
		head->x = x;
		head->y = y;
		head->next = NULL;
		head->prev = NULL;
	}
	else
	{
		tail->next = new Element();
		tail->next->prev = tail;
		tail = tail->next;
		tail->x = x;
		tail->y = y;
		tail->next = NULL;
	}
}

void Tabu::decrementCadency()
{
	Element* ptr = head;
	Element* ptrHelp;

	while (ptr != NULL) {
		ptr->cadency = ptr->cadency - 1;

		if (ptr->cadency == 0) {
			ptrHelp = ptr->next;
			sub(ptr);
			ptr = ptrHelp;
		}
		else
			ptr = ptr->next;	
	}

}

void Tabu::sub(Element * ptr)
{
	if (ptr == head)
		head = ptr->next;

	if (ptr == tail)
		tail = ptr->prev;

	Element* bufor = ptr->prev;
	
	if (ptr->prev != NULL)
		bufor->next = ptr->next;

	bufor = ptr->next;

	if (ptr->next != NULL)
		bufor->prev = ptr->prev;

	delete ptr;
}
