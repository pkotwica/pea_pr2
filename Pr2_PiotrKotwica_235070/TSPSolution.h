#pragma once
#include <iostream>
#include <random>


using namespace std;

class TSPSolution
{

	int* tab;
	int sizeOfTab;

private:
	int random();


public:
	TSPSolution(int sizeOfTab);
	TSPSolution(int* tab ,int sizeOfTab);
	~TSPSolution();


	void generateRandomSoltion();
	bool contains(int x);
	void swap(int idex0, int index1);
	int calculateSolution(int** matrix);
	int getNod(int index);
	int* getTab();
	void display(int sizeOfTab);
	void setTab(int* tab);

};

