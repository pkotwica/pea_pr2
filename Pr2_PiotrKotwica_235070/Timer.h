#pragma once
#include <windows.h>
#include <iostream>
#include <iomanip>

using namespace std;
	
class Timer
{
	long long int startTime;
	long long int frequency;
	long long int elapsed;
private:
	long long int read_QPC();


public:
	Timer();
	~Timer();

	void start();
	void stop();
	bool stop(float time);
	double getTimeSeconds();
};

