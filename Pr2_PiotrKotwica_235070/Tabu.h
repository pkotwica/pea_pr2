#pragma once
#include <iostream>

using namespace std;

class Tabu
{
	struct Element{
		int x = 0;
		int y = 0;
		int cadency = 10;

		Element* next = NULL;
		Element* prev = NULL;
	};

	Element* head;
	Element* tail;
	

public:
	Tabu(int sizeOfProblem);
	~Tabu();


	bool contains(int x, int y);
	void add(int x, int y);
	void decrementCadency();
	void sub(Element* ptr);

};

