#include "pch.h"
#include <iostream>
#include "SprawozdanieMenu.h"
#include "TestMenu.h"

using namespace std;

bool CzyPoprawnaDecyzja(int, int);


int main()
{
	int decyzja;
	bool czyKoniec = false;
	SprawozdanieMenu *sprawozdanieMenu = new SprawozdanieMenu();
	TestMenu *testMenu = new TestMenu();



	while (!czyKoniec)
	{
		system("cls");
		cout << "PROJEKTOWANIE EFEKTYWNYCH ALGORYTMOW" << endl;
		cout << "PROJEKT 1 - PROBLEM KOMIWOJAZERA (TSP)" << endl;
		cout << "______________________________________" << endl << endl;

		cout << "Wybierz sposob rozwiazania problemu: " << endl;
		cout << "1 - Testowanie" << endl;
		cout << "2 - Do sprawozdania" << endl;
		cout << "3 - WYJSCIE" << endl;
		cout << "Podaj numer: ";
		cin >> decyzja;

		do
		{
			if (CzyPoprawnaDecyzja(3, decyzja))
				break;

			cout << "Zly numer!" << endl;
			cout << "Podaj numer: ";
			cin >> decyzja;

		} while (!CzyPoprawnaDecyzja(4, decyzja));


		switch (decyzja)
		{
		case 1:
			testMenu->menu();
			break;
		case 2:
			sprawozdanieMenu->menu();
			break;
		case 3:
			czyKoniec = true;
			break;
		}

	}


	delete testMenu;
	delete sprawozdanieMenu;

	return 0;
}






bool CzyPoprawnaDecyzja(int max, int decyzja)
{
	if (decyzja > max || decyzja < 1)
		return false;
	else
		return true;
}
