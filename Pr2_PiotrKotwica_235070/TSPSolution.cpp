#include "pch.h"
#include "TSPSolution.h"


TSPSolution::TSPSolution(int sizeOfTab)
{
	this->sizeOfTab = sizeOfTab;
	tab = new int[sizeOfTab];

	for (int i = 0; i < sizeOfTab; i++)
		tab[i] = i;
}

TSPSolution::TSPSolution(int * tab, int sizeOfTab)
{
	this->sizeOfTab = sizeOfTab;
	this->tab = new int[sizeOfTab];

	for (int i = 0; i < sizeOfTab; i++)
		this->tab[i] = tab[i];
}


TSPSolution::~TSPSolution()
{
	if (tab != NULL)
		delete[] tab;

}

void TSPSolution::generateRandomSoltion()
{
	for (int i = 0; i < 2 * sizeOfTab; i++)
		swap(random(), random());
}

bool TSPSolution::contains(int x)
{
	for (int i = 1; i < sizeOfTab; i++)
		if (tab[i] == x)
			return true;

	return false;
}

void TSPSolution::swap(int index0, int index1)
{
	int bufor = tab[index0];
	tab[index0] = tab[index1];
	tab[index1] = bufor;
}

int TSPSolution::calculateSolution(int ** matrix)
{
	int solution = 0;

	solution += matrix[tab[sizeOfTab-1]][0];

	for (int i = 0; i < sizeOfTab - 1; i++)
		solution += matrix[tab[i]][tab[i + 1]];

	return solution;
}

int TSPSolution::getNod(int index)
{
	return tab[index];
}

int * TSPSolution::getTab()
{
	return tab;
}

void TSPSolution::display(int size)
{
	for (int i = 0; i < size; i++) 
		cout << tab[i] << " -> ";

	cout << tab[0];
}

void TSPSolution::setTab(int * tab)
{
	for (int i = 0; i < sizeOfTab; i++)
		this->tab[i] = tab[i];
}


int TSPSolution::random()
{
	random_device rd; // non-deterministic generator
	mt19937 gen(rd()); // random engine seeded with rd()
	uniform_int_distribution<> dist(1, (sizeOfTab-1)); // distribute results between
	 // 1 and 1000000 inclusive

	return dist(gen);
}
