#include "pch.h"
#include "TestMenu.h"


TestMenu::TestMenu()
{
	miasto = NULL;
	liczbaMiast = 0;
	kryterium_stopu = 5;
	dywersyfikacja = false;
	minimumOptimum = 0;
}


TestMenu::~TestMenu()
{
	resetCity();
}

void TestMenu::resetCity()
{
	if (liczbaMiast != 0 && miasto != NULL)
	{
		for (int i = 0; i < liczbaMiast; i++)
			delete[] miasto[i]; //uwolnienie pamieci
		delete[] miasto; //uwolnienie pamieci

		miasto = NULL;
		liczbaMiast = 0;
	}
}


void TestMenu::menu()
{
	int decyzja;
	bool czyKoniec = false;
	string filename;

	while (!czyKoniec)
	{
		system("cls");
		cout << "PROJEKT 1 - PROBLEM KOMIWOJAZERA (TSP)" << endl;
		cout << "MENU DO TESTOWANIA" << endl;
		cout << "______________________________________" << endl << endl;

		cout << "Wybierz opcje" << endl;
		cout << "1 - Wczytaj dane z pliku" << endl;
		cout << "2 - Wyswietl graf" << endl;
		cout << "3 - Wprowadz kryterium stopu (Ustwione: " << kryterium_stopu << " s)" << endl;
		cout << "4 - Dywersywikacja ";
		if (dywersyfikacja == true)
			cout << "(wlaczona)" << endl;
		else
			cout << "(wylaczona)" << endl;
		cout << "5 - Uruchom algorytm Symulownego Wyzarzania" << endl;
		cout << "6 - Uruchom algorytm Tabu Search" << endl;
		cout << "7 - WYJDZ" << endl;
		cout << "Podaj numer: ";
		cin >> decyzja;

		do
		{
			if (czyPoprawnaDecyzja(8, decyzja))
				break;

			cout << "Zly numer!" << endl;
			cout << "Podaj numer: ";
			cin >> decyzja;

		} while (!czyPoprawnaDecyzja(8, decyzja));


		switch (decyzja)
		{
		case 1:
			cout << "Podaj nazwe pliku: ";
			cin >> filename;
			wczytajZPliku(filename);
			cout << "\nPlik zostal wczytany.\n";
			system("pause");
			break;
		case 2:
			displayCity();
			break;
		case 3:
			cout << endl << "Podaj czas dla kryterium stopu (sekundy): ";
			cin >> kryterium_stopu;
			break;
		case 4:
			if (dywersyfikacja == true)
				dywersyfikacja = false;
			else
				dywersyfikacja = true;
			break;
		case 5:
			symulawaneWyzarzanie();
			break;
		case 6:
			tabuSerach();
			break;
		case 7:
			czyKoniec = true;
			break;
		}
	}
}

void TestMenu::wczytajZPliku(string file_name)
{
	ifstream file;
	file.open(file_name.c_str());

	if (file.is_open())
	{
		resetCity();
		if (file_read_line(file, liczbaMiast))
		{
			miasto = new int *[liczbaMiast];			//tworzenie odpowiednio duzej macierzy
			for (int i = 0; i < liczbaMiast; i++)
				miasto[i] = new int[liczbaMiast];

			int *tab = new int[liczbaMiast];

			for (int i = 0; i < liczbaMiast; i++)
				if (file_read_line(file, tab, liczbaMiast))
				{
					for (int j = 0; j < liczbaMiast; j++)
						miasto[i][j] = tab[j];
				}
				else
				{
					cout << "File error - READ EDGE" << endl;
					break;
				}

			delete[] tab;
		}
		else
			cout << "File error - READ INFO" << endl;
		file.close();
	}
	else
		cout << "File error - OPEN" << endl;
	cout << endl << endl << "Podaj najlepsze rozwiazanie: ";
	cin >> minimumOptimum;

}


bool TestMenu::file_read_line(ifstream &file, int tab[], int size)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	for (int i = 0; i < size; i++)
	{
		in_ss >> tab[i];
		if (in_ss.fail())
			return(false);
	}
	return(true);
}


bool TestMenu::file_read_line(ifstream &file, int &edges)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	in_ss >> edges;
	if (in_ss.fail())
		return(false);

	return(true);
}



void TestMenu::displayCity() {

	if (miasto != NULL && liczbaMiast != 0) {
		cout << endl << endl;
		for (int i = 0; i < liczbaMiast; i++) {
			cout << "| ";

			for (int j = 0; j < liczbaMiast; j++) {
				cout << miasto[i][j];

				if (j != (liczbaMiast - 1))
					cout << "\t";
				else
					cout << " |\n";
			}
		}
	}
	else
	{
		cout << endl << endl << "Nie wczytano pliku!";
	}
	system("pause");
}




bool TestMenu::czyPoprawnaDecyzja(int max, int decyzja)
{
	if (decyzja > max || decyzja < 1)
		return false;
	else
		return true;
}


long long int TestMenu::read_QPC()
{
	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return((long long int)count.QuadPart);
}

void TestMenu::symulawaneWyzarzanie()
{
	SimulatedAnnealing* simulatedAnnealing = new SimulatedAnnealing(miasto, liczbaMiast);
	TSPSolution* solution;

	solution = simulatedAnnealing->solveTSP(kryterium_stopu);

	int waga = solution->calculateSolution(miasto);

	cout << "Droga: ";
	solution->display(liczbaMiast);
	cout << endl << "Waga: " << waga << endl;
	cout << endl << "Rozwiazanie: " << minimumOptimum << endl;

	float bladWzgledny = (float)(waga - minimumOptimum) / (float)minimumOptimum;
	bladWzgledny = bladWzgledny * 100.0;
	cout << endl << "Blad wzgledny: " << fixed << setprecision(2) << bladWzgledny << "%" << endl << endl;

	system("pause");
	system("pause");

	delete solution;
	delete simulatedAnnealing;

}

void TestMenu::tabuSerach()
{
	TabuSearch* tabuSearch = new TabuSearch(miasto, liczbaMiast);
	TSPSolution* solution;

	solution = tabuSearch->solveTSP(kryterium_stopu, dywersyfikacja);

	int waga = solution->calculateSolution(miasto);
	
	cout << "Droga: ";
	solution->display(liczbaMiast);
	cout << endl << "Waga: " << waga << endl;
	cout << endl << "Rozwiazanie: " << minimumOptimum << endl;

	float bladWzgledny = (float)(waga - minimumOptimum) / (float)minimumOptimum;
	bladWzgledny = bladWzgledny * 100.0;
	cout << endl << "Blad wzgledny: " << fixed << setprecision(2) << bladWzgledny << "%" << endl << endl;

	system("pause");
	system("pause");

	delete solution;
	delete tabuSearch;
}