#pragma once
#include <iostream>
#include <random>
#include <cmath>
#include "TSPSolution.h"
#include "Timer.h"
#include "Tabu.h"

using namespace std;


class SimulatedAnnealing
{
	Timer* timer;
	int** matrix;
	int sizeOfMatrix;
	float temperature;
	float decreaser;

public:
	SimulatedAnnealing(int** matrix, int sizeOfMatrix);
	~SimulatedAnnealing();

	TSPSolution* solveTSP(double timeToStop);
	void decreaseTempertaure();
	TSPSolution* randMove(TSPSolution* x);
	float randProbality();
	float propability(TSPSolution* x, TSPSolution* y);
	int random(int from, int to);
};

