#include "pch.h"
#include "TabuSearch.h"


TabuSearch::TabuSearch(int** matrix, int sizeOfMatrix)
{
	timer = new Timer();
	this->matrix = matrix;
	this->sizeOfMatrix = sizeOfMatrix;
	this->limitForDiversification = sizeOfMatrix;
}


TabuSearch::~TabuSearch()
{
	if (timer != NULL)
		delete timer;
}

TSPSolution* TabuSearch::solveTSP(double timeToStop, bool dywersyfikacja)
{
	Tabu* tabuList = new Tabu(sizeOfMatrix);
	TSPSolution* x = NULL;
	TSPSolution* y = NULL;
	TSPSolution* optimum = NULL;
	int optimumSolution;
	int counterForDiversification = 0;

	x = new TSPSolution(sizeOfMatrix);
	x->generateRandomSoltion();
	optimum = x;
	optimumSolution = optimum->calculateSolution(matrix);
	timer->start();


	do {
		y = findNeightbour(tabuList, x, optimumSolution);

		if(x != optimum)
			delete x;
		x = y;


		if (optimumSolution > x->calculateSolution(matrix)) {
			delete optimum;
			optimum = x;
			optimumSolution = optimum->calculateSolution(matrix);
			counterForDiversification = 0;
		}
		else {													//dywersyfikacja
			if (dywersyfikacja) {
				counterForDiversification++;

				if (counterForDiversification > limitForDiversification)
					x->generateRandomSoltion();
			}
		}

		tabuList->decrementCadency();

	} while (!timer->stop(timeToStop));

	delete tabuList;
	return optimum;
}

TSPSolution * TabuSearch::findNeightbour(Tabu * tabuList, TSPSolution * x, int optimumSoltion)
{
	TSPSolution* bestY = new TSPSolution(sizeOfMatrix);
	TSPSolution* y = new TSPSolution(sizeOfMatrix);
	int bestResult = -99999;
	int currentResult;
	int nod1;
	int nod2;

	for (int i = 1; i < sizeOfMatrix; i++) {
		for (int j = 1; j < sizeOfMatrix; j++) {
			if (i == j)
				continue;

			y->setTab(x->getTab());
			y->swap(i, j);

			currentResult = (x->calculateSolution(matrix) - y->calculateSolution(matrix));

			if (tabuList->contains(y->getNod(i), y->getNod(j)) && currentResult >= optimumSoltion)					//kryterium aspiracji
				continue;

			
			if (bestResult < currentResult) {
				bestResult = currentResult;

				bestY->setTab(y->getTab());

				nod1 = y->getNod(i);
				nod2 = y->getNod(j);
			}
		}
	}

	tabuList->add(nod1, nod2);
	delete y;

	return bestY;
}


